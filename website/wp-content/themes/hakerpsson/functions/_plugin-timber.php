<?php

use Timber\Timber;
use Timber\Twig_Function;
Timber::$dirname = array('templates');

add_filter('timber/twig', function ($twig) {
    $twig->addFunction(new Twig_Function('renderPictureTag', 'renderPictureTag'));
    $twig->addFunction(new Twig_Function('renderImageTeaser', 'renderImageTeaser'));
    
    return $twig;
});
