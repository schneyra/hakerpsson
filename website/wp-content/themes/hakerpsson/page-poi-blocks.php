<?php
/**
* Template Name: Alle POI
*/
$posts = get_posts(['numberposts' => -1]);

//echo "<ul>";
$chapter = [];

foreach ($posts as $post) {
    if (has_blocks($post->post_content)) {
        $blocks = parse_blocks($post->post_content);

        foreach ($blocks as $block) {
            if ($block['blockName'] === 'carbon-fields/poi') {
                
                $data = $block['attrs']['data'];
                $lnglat = $data['map']['lng'] . ",". $data['map']['lat'];
                $id = str_replace([',','.', '-'], ['','', ''], $lnglat);

                $chapter[$id] = [
                    'bearing' => rand(0, 180),
                    'center' => [$data['map']['lng'], $data['map']['lat']],
                    'zoom' => rand(12, 17),
                    'pitch' => rand(0,40),
                    //'speed' => rand(5, 10) / 10
                ];

                $sections[] = [
                    'id' => $id,
                    'title' => $data['title'],
                    'description' => $data['description'],
                ];

        

                /*
                echo "<li>";
                echo "<h2>" . $block['attrs']['data']['title'] . "</h2>";
                echo "<p>" . $block['attrs']['data']['map']['value'] . "</p>";
                echo "<p><small>Aus dem Beitrag: " . $post->post_title . "</small></p>";
                echo "<img width='640' height='320' src='https://api.mapbox.com/styles/v1/mapbox/light-v10/static/pin-s-star+285A98(". $lnglat .")/". $lnglat .",11,0/640x320@2x?access_token=pk.eyJ1Ijoic2NobmV5cmEiLCJhIjoiY2l2NGFyajRhMDAwMjJ6cW1hdnRhem1sZiJ9.fytZp1WXg4T1n_AmngSPJA'>";
                echo "</li>";
                */
            }
        }
    }
};
/*
echo "<pre>";
var_dump(json_encode($chapter));
echo "</pre>";

die();
*/
//echo "</ul>";

//echo "<img width='640' height='320' src='https://api.mapbox.com/styles/v1/mapbox/light-v10/static/". implode(',', $marker) ."/auto/640x320@2x?access_token=pk.eyJ1Ijoic2NobmV5cmEiLCJhIjoiY2l2NGFyajRhMDAwMjJ6cW1hdnRhem1sZiJ9.fytZp1WXg4T1n_AmngSPJA'>";
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Fly to a location based on scroll position</title>
<meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no">
<link href="https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.css" rel="stylesheet">
<script src="https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.js"></script>
<style>
body { margin: 0; padding: 0; }
#map { position: absolute; top: 0; bottom: 0; width: 100%; }
</style>
</head>
<body>
<style>
    * {
        box-sizing: border-box;
    }

    #map {
        position: fixed;
        width: 50%;
    }
    #features {
        height: 100vh;
        width: 50%;
        margin-left: 50%;
        font-family: sans-serif;
        display: flex;
        flex-direction: column;
        overflow-y: auto;
        background-color: #fafafa;
        scroll-snap-type: y mandatory;
    }
    section {
        scroll-snap-align: start;
        height: 50vh;
        padding: 25px 50px;
        line-height: 25px;
        border-bottom: 1px solid #ddd;
        opacity: 0.25;
        font-size: 13px;
        flex-shrink: 0;
    }
    section.active {
        opacity: 1;
    }
    section:last-child {
        border-bottom: none;
        margin-bottom: 50vh;
    }
</style>

<div id="map"></div>
<div id="features">
    <?php $first = true; ?>
    <?php foreach ($sections as $section) : ?>

        <section id="<?php echo $section['id']; ?>" <?php if ($first) : ?>class="active"<?php endif; $first = false; ?>>
            <h3><?php echo $section['title']; ?></h3>
            <?php echo $section['description']; ?>
        </section>

    <?php endforeach; ?>
</div>

<?php $first = $chapter[$sections[0]['id']]; ?>

<script>
	mapboxgl.accessToken = 'pk.eyJ1Ijoic2NobmV5cmEiLCJhIjoiY2l2NGFyajRhMDAwMjJ6cW1hdnRhem1sZiJ9.fytZp1WXg4T1n_AmngSPJA';
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/light-v10',
        center: <?php echo json_encode($first['center']); ?>,
        zoom: <?php echo $first['zoom']; ?>,
        bearing: <?php echo $first['bearing']; ?>,
        pitch: <?php echo $first['pitch']; ?>
    });

    var chapters = <?php echo json_encode($chapter); ?>;

    // On every scroll event, check which element is on screen
    const featureWrapper = document.querySelector('#features');
    featureWrapper.onscroll = function () {
        var chapterNames = Object.keys(chapters);
        for (var i = 0; i < chapterNames.length; i++) {
            var chapterName = chapterNames[i];
            if (isElementOnScreen(chapterName)) {
                setActiveChapter(chapterName);
                break;
            }
        }
    };

    var activeChapterName = '<?php echo $sections[0]['id']; ?>';
    function setActiveChapter(chapterName) {
        if (chapterName === activeChapterName) return;

        map.flyTo(chapters[chapterName]);

        document.getElementById(chapterName).setAttribute('class', 'active');
        document.getElementById(activeChapterName).setAttribute('class', '');

        activeChapterName = chapterName;
    }

    function isElementOnScreen(id) {
        var element = document.getElementById(id);
        var bounds = element.getBoundingClientRect();
        return bounds.top < window.innerHeight && bounds.bottom > 0;
    }
</script>

</body>
</html>