/* gulp and config */
const { src, dest, series, watch } = require("gulp");
const rename = require("gulp-rename");
const sourcemaps = require("gulp-sourcemaps");

const config = {
    distPath: "./website/wp-content/themes/hakerpsson/dist",
    scssSourceFiles: ["./source/scss/*.scss"],
    scssWatchFiles: ["./source/scss/**/*.scss"],
    jsSourceFiles: [
        "./source/js/**/*.js",
        "./node_modules/vanilla-lazyload/dist/lazyload.iife.min.js",
        "./node_modules/prismjs/prism.js",
    ],
    jsWatchFiles: ["./source/js/**/*.js"],
};

/* SCSS to CSS */
const sass = require("gulp-sass")(require('sass'));
const autoprefixer = require("gulp-autoprefixer");

const buildScss = () => {
    return src(config.scssSourceFiles)
        .pipe(sourcemaps.init())
        .pipe(
            sass.sync({
                outputStyle: "compressed",
                includePaths: ["../node_modules"],
            }).on("error", sass.logError),
        )
        .pipe(autoprefixer())
        .pipe(rename({ suffix: ".min" }))
        .pipe(sourcemaps.write('.'))
        .pipe(dest(`${config.distPath}/css`));
};

/* JS */
const uglify = require("gulp-uglify-es").default;

const buildJs = () => {
    return src(config.jsSourceFiles)
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(rename({ suffix: ".min" }))
        .pipe(sourcemaps.write('.'))
        .pipe(dest(`${config.distPath}/js`));
};

/* Hash Files and generate manifest */
const hash = require("gulp-hash");

const generateManifest = () => {
    return src([
        `${config.distPath}/**/*.min.css`,
        `${config.distPath}/**/*.min.js`,
        `!${config.distPath}/*.json`,
    ])
        .pipe(hash())
        .pipe(dest(`${config.distPath}`))
        .pipe(
            hash.manifest(`./manifest.json`, {
                deleteOld: true,
                sourceDir: config.distPath,
            }),
        )
        .pipe(dest(config.distPath));
};

/* gulp-functions */
exports.watch = () => {
    watch(
        `${config.scssWatchFiles}`,
        { ignoreInitial: false },
        series(buildScss, generateManifest),
    );

    watch(
        `${config.jsWatchFiles}`,
        { ignoreInitial: false },
        series(buildJs, generateManifest),
    );
};

exports.default = series(buildScss, buildJs, generateManifest);
