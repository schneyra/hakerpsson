<?php

use Timber\PostQuery;
use Timber\Timber;
use Timber\Term;

$context                = Timber::context();
$context['posts']       = new PostQuery();
$context['currentPage'] = get_query_var('paged') ? get_query_var('paged') : 1;

$context['title'] = 'Archiv';

if (is_day()) {
	$context['title'] = 'Archiv: ' . get_the_date('d. F Y');
} elseif (is_month()) {
	$context['title'] = 'Archiv: ' . get_the_date('F Y');
} elseif (is_year()) {
	$context['title'] = 'Archiv: ' . get_the_date('Y');
} elseif (is_tag()) {
	$context['title'] = single_tag_title('', false);
}

Timber::render('archive.html.twig', $context);
