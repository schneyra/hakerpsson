<?php

use Carbon_Fields\Block;
use Carbon_Fields\Field;

add_action('carbon_fields_register_fields', static function() {
    Block::make(__('POI'))
    ->add_fields(array(
        Field::make('text', 'title', __('Titel')),
        Field::make('image', 'image', __('Bild')),
        Field::make('rich_text', 'description', __('Beschreibung')),
        Field::make('text', 'website', __('Webseite')),
        Field::make('map', 'map', __('Karte')),
        ))
        ->set_category('hakerpsson', __('Hak Erpsson'), null)
        ->set_icon('location-alt')
        ->set_render_callback(function($fields, $attributes) {
            $context = $fields;

            $fields['map']['lng'] = round($fields['map']['lng'], 10);
            $fields['map']['lat'] = round($fields['map']['lat'], 10);
            $context['lnglat'] = $fields['map']['lng'] . "," . $fields['map']['lat'];

            $context['alignmentClass'] = 'alignright';
            if (isset($attributes) && isset($attributes['className'])) {
                if (strpos($attributes['className'], 'align') > -1) {
                    $context['alignmentClass'] = $attributes['className'];
                }
            }

            $context['slug'] = sanitize_title($context['title']);

            Timber::render('_gutenberg-poi.html.twig', $context);
        });
});