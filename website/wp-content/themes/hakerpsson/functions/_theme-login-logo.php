<?php

add_action( 'login_enqueue_scripts', static function () { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/login-logo.jpg);
            height: 90px;
            width: 90px;
            background-size: 90px 90px;
            background-repeat: no-repeat;
            border-radius: 50%;
            border: 2px solid white;
            box-shadow: 0 0 2px #7e8993;
        }
    </style>
<?php } );
