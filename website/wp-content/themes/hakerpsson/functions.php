<?php

require_once(get_template_directory() . '/functions/_theme-helper.php');
require_once(get_template_directory() . '/functions/_theme-menus.php');
require_once(get_template_directory() . '/functions/_theme-images.php');
require_once(get_template_directory() . '/functions/_theme-support.php');
require_once(get_template_directory() . '/functions/_theme-assets.php');
require_once(get_template_directory() . '/functions/_theme-cleanup.php');
require_once(get_template_directory() . '/functions/_theme-pagination.php');
require_once(get_template_directory() . '/functions/_theme-login-logo.php');
require_once(get_template_directory() . '/functions/_theme-reorder-reisen-subcategories.php');

require_once(get_template_directory() . '/functions/_plugin-timber.php');
require_once(get_template_directory() . '/functions/_plugin-timber-picture.php');
require_once(get_template_directory() . '/functions/_plugin-timber-teaser.php');
require_once(get_template_directory() . '/functions/_plugin-carbon-fields.php');

require_once(get_template_directory() . '/functions/_gutenberg-block-config.php');
require_once(get_template_directory() . '/functions/_gutenberg-allowed-blocks.php');
require_once(get_template_directory() . '/functions/_gutenberg-embed.php');
require_once(get_template_directory() . '/functions/_gutenberg-image.php');
require_once(get_template_directory() . '/functions/_gutenberg-gallery.php');
require_once(get_template_directory() . '/functions/_gutenberg-heading.php');
require_once(get_template_directory() . '/functions/_gutenberg-code.php');
require_once(get_template_directory() . '/functions/_gutenberg-poi.php');
require_once(get_template_directory() . '/functions/_gutenberg-categories.php');
require_once(get_template_directory() . '/functions/_gutenberg-archives.php');
