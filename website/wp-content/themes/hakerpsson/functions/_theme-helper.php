<?php

function is_rest() {
    return (defined('REST_REQUEST') && REST_REQUEST);
}

function he_dump($content) {
    echo "<pre style='line-height: 1.2rem;'>";
    var_dump($content);
    echo "</pre>";
}
