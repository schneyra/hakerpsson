<?php
if ( ! is_admin() ) {
	add_action( 'pre_get_posts', function ( $query ) {
		if ( ! $query->is_main_query() ) {
			return $query;
		}

		if ( ! is_category() ) {
			return $query;
		}

		$reisenId = get_cat_id( 'reisen' );

		/**
		 * Sortiert Reisen-Subcategorien um und gibt alle Beiträge auf einer Seite aus
		 */
		$reisenSubcategoryId = get_queried_object()->term_id;

		if ( cat_is_ancestor_of( $reisenId, $reisenSubcategoryId ) ) {
			$query->set( 'order', 'ASC' );
			$query->set( 'posts_per_page', '-1' );
		}

		/**
		 * Auf der Kategorie-Übersicht "Reisen" sollen nur die Posts angezeigt werden,
		 * die nicht in einer Unterkategorie stecken
		 */
		if ( is_category( $reisenId ) ) {
			$queried_object = get_queried_object();
			$child_cats     = (array) get_term_children( $queried_object->term_id, 'category' );

			$query->set( 'category__not_in', array_merge( $child_cats ) );
		}

		return $query;
	} );
}
