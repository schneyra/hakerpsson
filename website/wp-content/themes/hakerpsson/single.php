<?php

use Timber\Post;
use Timber\Image;
use Timber\Timber;

$context                     = Timber::context();
$context['post']             = new Post();
$context['isSingle']         = true;
$context['logoId']           = get_theme_mod('custom_logo');

/**
 * Related Posts
 */
$relatedPosts = [];

if (function_exists('get_crp_posts_id')) {
        $relatedPostIds = get_crp_posts_id(array('postid' => $context['post']->ID, 'limit' => 10));
        
        /*if (!sizeof($relatedPostIds)) {
                $relatedPostIds = [(object) ['ID' => 91],(object) ['ID' => 70],(object) ['ID' => 54],(object) ['ID' => 42]];
        }*/
        
        foreach ($relatedPostIds as $relatedPostId) {
		$relatedPosts[] = new Post($relatedPostId->ID);
	}

	$context['relatedPosts'] = $relatedPosts;
}

/**
 * Post Thumbnail
 */
$context['hasPostThumbnail'] = get_post_thumbnail_id() ? true : false;
$context['postThumbnailId']  = get_post_thumbnail_id();

if ($context['hasPostThumbnail']) {
        enqueueIntersectionObserver();
}

/*
 * Data for JSON-LD
 */
if ( ! $context['post']->thumbnail ) {
        $context['jsonLdImage'] = new Image( DEFAULT_IMAGE_ID );
} else {
        $context['jsonLdImage'] = $context['post']->thumbnail;
}

Timber::render( 'single.html.twig', $context );
