<?php

add_filter('carbon_fields_map_field_api_key', static function($key) {
    return GOOGLE_MAPS_API_KEY;
});
