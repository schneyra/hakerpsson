<?php

use Timber\Timber;

if (!is_admin()) {
	add_filter('render_block_core/gallery', function ($content, $block) {
		if (is_feed()) {
			return $content;
		}

		return renderGalleryBlock($content, $block);
	}, 10, 2);
}

function renderGalleryBlock($content, $block) {
    return $content;

    /*
	$images  = [];
	$context = [];
	$columns = $block['attrs']['columns'] ?? 2;

	$dom = new DOMDocument();
    libxml_use_internal_errors( true );
    $dom->loadHTML( mb_convert_encoding( $content, 'HTML-ENTITIES', 'UTF-8' ), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );
	libxml_use_internal_errors( false );
	
	foreach ($dom->getElementsByTagName('figure') as $node) {
		if (strpos($node->getAttribute('class'), 'wp-block-gallery') > -1){
			$context['blockClasses'] = $node->getAttribute('class');
        } else {
            $imageNode = $node->getElementsByTagName('img')[0];
			$figcaptionNode = $node->getElementsByTagName('figcaption')[0];

			if ($imageNode) {
				$id = $imageNode->getAttribute('data-id');
				$images[$id] = [];
				$images[$id]['id'] = $imageNode->getAttribute('data-id');
				$images[$id]['alt'] = $imageNode->getAttribute('alt');
				$images[$id]['sizes'] = '(min-width: 1000px) 1000px, 100vw';

				if ($figcaptionNode) {
					$images[$id]['caption'] = $figcaptionNode->textContent;
				}
			}
		}
	}

	$context['images'] = $images;

	return Timber::compile( '_gutenberg-gallery.html.twig', $context );
    */
};
