<?php
// Allow images with width > 1600px in srcset
// see: http://wordpress.stackexchange.com/questions/224198/one-of-my-image-sizes-isnt-showing-up-in-srcset
add_filter('max_srcset_image_width', function ($max_srcset_image_width, $size_array) {
	return 2500;
}, 10, 2);

// Remove all image-sizes except thumbnail
remove_image_size('medium');
remove_image_size('medium_large');
remove_image_size('large');
remove_image_size('1536x1536');
remove_image_size('2048x2048');
remove_image_size('crp_thumbnail');
