<?php

use Timber\Post;
use Timber\Timber;
use Timber\Image;

$context           = Timber::context();
$context['post']   = new Post();
$context['isPage'] = true;
$context['logoId']           = get_theme_mod('custom_logo');

/**
 * Post Thumbnail
 */
$context['hasPostThumbnail'] = get_post_thumbnail_id() ? true : false;
$context['postThumbnailId']  = get_post_thumbnail_id();

if ($context['hasPostThumbnail']) {
        enqueueIntersectionObserver();
}

/*
 * Daten für JSON-LD
 */
if (!$context['post']->thumbnail) {
        $context['jsonLdImage'] = new Image(DEFAULT_IMAGE_ID);
} else {
        $context['jsonLdImage'] = $context['post']->thumbnail;
}

Timber::render('page.html.twig', $context);
