(() => {
    const embedPlayButtons = document.querySelectorAll(".js-embed-play");

    if (embedPlayButtons) {
        embedPlayButtons.forEach((playButton) => {
            const wrapper = playButton.closest(".js-embed-wrapper");
            const iframe = wrapper.querySelector(".js-embed-iframe");

            playButton.addEventListener("click", () => {
                wrapper.classList.remove("has-playbutton");
                iframe.setAttribute("src", iframe.getAttribute("data-src"));
                iframe.removeAttribute("data-src");
            });
        });
    }
})();
