<?php

if (!is_admin()) {
	add_filter('render_block_core/heading', function ($content, $block) {
		if (is_feed()) {
			return $content;
		}

		return renderHeadingBlock($content, $block);
	}, 10, 2);
}

function renderHeadingBlock($content, $block) {
	if (is_single() || is_rest()) {
		$headingOrder = 'h2';
		$headingClass = 'heading--2';

		if (isset($block['attrs']) && isset($block['attrs']['level'])) {
			switch($block['attrs']['level']) {
				case 1: 
					$headingOrder = 'h2';
					$headingClass = 'heading--2';
					break;
				// in case of h2, there is no level-information
				case 3: 
					$headingOrder = 'h3';
					$headingClass = 'heading--3';
					break;
				case 4: 
					$headingOrder = 'h4';
					$headingClass = 'heading--4';
					break;
				case 5:
					$headingOrder = 'h5';
					$headingClass = 'heading--5';
					break;
				case 6: 
					$headingOrder = 'h6';
					$headingClass = 'heading--6';
					break;
			}
		}
	} else {
		$headingOrder = 'h3';
		$headingClass = 'heading--2';

		if (isset($block['attrs']) && isset($block['attrs']['level'])) {
			switch($block['attrs']['level']) {
				case '1': 
					$headingOrder = 'h3';
					$headingClass = 'heading--2';
					break;
				// in case of h2, there is no level-information
				case 3: 
					$headingOrder = 'h4';
					$headingClass = 'heading--3';
					break;
				case 4: 
					$headingOrder = 'h5';
					$headingClass = 'heading--4';
					break;
				case 5:
					$headingOrder = 'h6';
					$headingClass = 'heading--5';
					break;
				case 6: 
					$headingOrder = 'p';
					$headingClass = 'heading--6';
					break;
			}
		}
	}

	$content = trim(strip_tags($block['innerHTML']));

	$context['content'] = $content;
	$context['customClassName'] = $block['attrs']['className'] ?? '';
	$context['headingOrder'] = $headingOrder;
	$context['headingClass'] = $headingClass;
	return Timber::compile('_gutenberg-heading.html.twig', $context);
};
