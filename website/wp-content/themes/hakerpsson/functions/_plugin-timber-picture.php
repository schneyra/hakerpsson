<?php 
/**
 * Generiert ein `<picture>`-Tag
 * 
 * $attributes
 */
function renderPictureTag($attributes) {
    $defaultAttributes = [
        'imagePath' => null,
        'imageId' => null,
        'alt' => '',
        'align' => 'default',
        'width' => null,
        'height' => null,
        'blur' => 0,
        'pictureElementClasses' => '',
        'imageElementClasses' => '',
        'sizes' => null,
        'imageWidthsToGenerate' => null,
        'lazyloading' => true,
    ];

    $attributes = array_merge($defaultAttributes, $attributes);

    if (!$attributes['imagePath'] && !$attributes['imageId']) {
        return;
    }

    // if an image id is set, eventually also given path should be ignored
    if ($attributes['imageId']) {
        $timberImage = new Timber\Image($attributes['imageId']);

        $attributes['width'] = $timberImage->width;
        $attributes['height'] = $timberImage->height;
        $attributes['imagePath'] = $timberImage->file;

        /**
         * Ohne Timber würde das auch so gehen:
         * $image = wp_get_attachment_image_src($attributes['imageId'], 'full');
         * $attributes['width'] = $image[1];
         * $attributes['height'] = $image[2];
         * 
         * preg_match("/.*\/wp-content\/uploads\/(.*\/.*\/.*)/", $image[0], $matches);
         * $attributes['imagePath'] = $matches[0] ? $matches[1] : null;
         */
    }

    // imagePath will be used for video-cover mainly
    if ($attributes['imagePath'] && (strpos($attributes['imagePath'], 'youtube/') > -1 || strpos($attributes['imagePath'], 'vimeo/') > -1)) {        
        $attributes['width'] = 16;
        $attributes['height'] = 9;
    }

    if ($attributes['imagePath'] && strpos($attributes['imagePath'], 'spotify/') > -1) {        
        $attributes['width'] = 1;
        $attributes['height'] = 1;
    }


    if ($attributes['imagePath'] && strpos($attributes['imagePath'], 'theme/fallback') > -1) {        
        $attributes['width'] = 2000;
        $attributes['height'] = 1342;
    }

    // SIZES
    // possible alignments: (left|right), default, wide, full
    if (!$attributes['sizes']) {
        switch ($attributes['align']) {
            case 'left':
            case 'right':
                $attributes['sizes'] = '(min-width: 1000px) 625px, 100vw';
                break;
            case 'wide':
                $attributes['sizes'] = '(min-width: 1600px) 1600px, 100vw';
                break;
            case 'full':
                $attributes['sizes'] = '(min-width: 2500px) 2500px, 100vw';
                break;
            default:
                $attributes['sizes'] = '(min-width: 1000px) 1000px, 100vw';
        }
    }

    if ($attributes['lazyloading']) {
        $attributes['imageElementClasses'] .= ' js-lazyload'; 
    }

    // PLACEHOLDER
    $attributes['placeholder'] = "/images/100/0/500/{$attributes['imagePath']}.jpg";

    // SRCSETS
    // these image widths will be added to the srcset-attribute
    if (!$attributes['imageWidthsToGenerate']) {
        $attributes['imageWidthsToGenerate'] = [320, 625, 840, 1000, 1250, 1600, 2500];
    }

    // Image-Formats must be in specific order, JPG last
    $imageFormats = ['avif', 'webp', 'jpg'];

    $attributes['srcsets'] = [];

    foreach ($imageFormats as $format) {
        foreach ($attributes['imageWidthsToGenerate'] as $width) {
            $attributes['srcsets'][$format][] = "/images/{$width}/0/{$attributes['blur']}/{$attributes['imagePath']}.{$format} {$width}w";
        }
    }

    return Timber::compile('_picture.html.twig', $attributes);
};
