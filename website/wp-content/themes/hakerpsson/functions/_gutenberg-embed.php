<?php

use Timber\Image;
use Timber\Timber;

if (!is_admin()) {
	add_filter('render_block', function ($content, $block) {
		if (is_feed()) {
			return $content;
		}

		if ($block['blockName'] === 'core-embed/youtube' || ($block['blockName'] === 'core/embed' && $block['attrs']['providerNameSlug'] === 'youtube')) {
			loadEmbedJs();
			return youTubeEmbed($content, $block);
		}

		if ($block['blockName'] === 'core-embed/vimeo' || ($block['blockName'] === 'core/embed' && $block['attrs']['providerNameSlug'] === 'vimeo')) {
			loadEmbedJs();
			return vimeoEmbed($content, $block);
		}

		if ($block['blockName'] === 'core-embed/spotify' || ($block['blockName'] === 'core/embed' && $block['attrs']['providerNameSlug'] === 'spotify')) {
			loadEmbedJs();
			return spotifyEmbed($content, $block);
		}

		return $content;
	}, 10, 2);
}

function vimeoEmbed($content, $block) {
	$url = $block['attrs']['url'];
	$transientNameHash = wp_hash('hakerpsson_vimeo_' . $url);
	$transient         = get_transient($transientNameHash);

	if (!empty($transient)) {
		$videoData = $transient;
	} else {
		$vimeoApiUrl = "https://vimeo.com/api/oembed.json?url=" . $url . "&width=2500";
		$apiResponse = wp_remote_get($vimeoApiUrl);
		$videoData   = json_decode($apiResponse['body']);

		set_transient($transientNameHash, $videoData, MONTH_IN_SECONDS);
	}

	$video['className'] = $block['attrs']['className'] ?? '';
	$video['halfsize'] = false;
	$video['align'] = null;

	if (isset($block['attrs']['align'])) {
		$video['className'] .= ' align' . $block['attrs']['align'];
		$video['align'] = $block['attrs']['align'];

		if ($block['attrs']['align'] === 'left' || $block['attrs']['align'] === 'right') {
			$video['halfsize'] = true;
		} else {
			$video['className'] .= ' wp-block-embed';
		}
	}

	$video['title'] = $videoData->title;
	$video['id']    = $videoData->video_id;
	$video['image'] = new Image($videoData->thumbnail_url);

	$context          = [];
	$context['video'] = $video;
	
	return Timber::compile('_gutenberg-embed-vimeo.html.twig', $context);
}

function youTubeEmbed($content, $block) {
	$url = $block['attrs']['url'];
	$transientNameHash = wp_hash('hakerpsson_youtube_' . $url);
	$transient         = get_transient($transientNameHash);

	parse_str(parse_url($url, PHP_URL_QUERY), $urlVariables);
	$video['id'] = $urlVariables['v'];

	if (!empty($transient)) {
		$videoData = $transient;
	} else {
		$youtubeApiUrl = "https://www.googleapis.com/youtube/v3/videos?id=" . $video['id'] . "&key=AIzaSyB3rbK1yjLj7OehaIe0DylpF_n3ZFjhm0Y&part=snippet";
		$apiResponse   = wp_remote_get($youtubeApiUrl);
		$videoData     = json_decode($apiResponse['body']);

		set_transient($transientNameHash, $videoData, MONTH_IN_SECONDS);
	}

	$video['className'] = $block['attrs']['className'] ?? '';
	$video['halfsize'] = false;
	$video['align'] = null;

	if (isset($block['attrs']['align'])) {
		$video['className'] .= ' align' . $block['attrs']['align'];
		$video['align'] = $block['attrs']['align'];

		if ($block['attrs']['align'] === 'left' || $block['attrs']['align'] === 'right') {
			$video['halfsize'] = true;
		} else {
			$video['className'] .= ' wp-block-embed';
		}
	}

	$video['title'] = $videoData->items[0]->snippet->title;

	$context          = [];
	$context['video'] = $video;

	return Timber::compile('_gutenberg-embed-youtube.html.twig', $context);
}

function spotifyEmbed($content, $block) {
	$url = $block['attrs']['url'];
	$transientNameHash = wp_hash('hakerpsson_spotify_' . $url);
	$transient         = get_transient($transientNameHash);

	if (!empty($transient)) {
		$playlistData = $transient;
	} else {
		$spotifyApiUrl = "https://embed.spotify.com/oembed?url=" . $url;
		$apiResponse   = wp_remote_get($spotifyApiUrl);
		$playlistData  = json_decode($apiResponse['body']);

		set_transient($transientNameHash, $playlistData, MONTH_IN_SECONDS);
	}

	$playlist              = [];
	$playlist['className'] = $block['attrs']['className'] ?? '';
	$playlist['halfsize']  = false;
	$playlist['align']     = null;

	if (isset($block['attrs']['align'])) {
		$playlist['className'] .= ' align' . $block['attrs']['align'];
		$playlist['align'] = $block['attrs']['align'];

		if ($block['attrs']['align'] === 'left' || $block['attrs']['align'] === 'right') {
			$playlist['halfsize'] = true;
		} else {
			$playlist['className'] .= ' wp-block-embed';
		}
	}

	$html = $playlistData->html;
	$html = str_replace(array(
		'src=',
		'<iframe'
	), array(
		'data-src=',
		'<iframe class="wp-block-embed__iframe js-embed-iframe"'
	), $html);

	$playlist['html']  = $html;
	$playlist['title'] = $playlistData->title;
	$playlist['image'] = $playlistData->thumbnail_url;

	$context             = [];
	$context['playlist'] = $playlist;

	return Timber::compile('_gutenberg-embed-spotify.html.twig', $context);
}

function loadEmbedJs() {
	if (file_exists(get_template_directory() . '/dist/manifest.json')) {
		$manifest = file_get_contents(get_template_directory() . '/dist/manifest.json');
		$manifest = json_decode($manifest, true);

		if (isset( $manifest['js/embed.min.js'])) {
			$jsFileUrl = get_template_directory_uri() . '/dist/' . $manifest['js/embed.min.js'];
			wp_enqueue_script('embed', $jsFileUrl, [], null, true);
		}
	}
}