<?php

use Timber\Image;
use Timber\Menu;
use Timber\Timber;

$context = Timber::context();

// Überschrift auf der Startseite als `h1` auszeichnen, sonst als `div`
$context['headlineContainerOpeningElement'] = '<div>';
$context['headlineContainerClosingElement'] = '</div>';

if ( is_home() ) {
	$context['headlineContainerOpeningElement'] = '<h1>';
	$context['headlineContainerClosingElement'] = '</h1>';
}

$context['mainMenu'] = new Menu( 'main' );

Timber::render( '_header.html.twig', $context );
