<?php

use Timber\Image;
use Timber\Timber;

function renderImageTeaser($post = null, $type = 'image') {
    if (!$post) {
        return '';
    }

    $imageTeaserContext = [];
    $imageTeaserContext['post'] = $post;
    $imageTeaserContext['imageId'] = get_post_thumbnail_id($post);
    $imageTeaserContext['imagePath'] = null;

    // Wenn der Artikel kein Beitragsbild hat, suchen wir nach einem Image-Block
    if (!$imageTeaserContext['imageId']) {
        $imageTeaserContext['imageId'] = getFirstImageIdFromBlocks($post);
    }

    // Wenn der Artikel dann immer noch kein Beitragsbild hat, 
    // suchen wir nach einem Embed-Block, um das Cover abzugreifen
    if (!$imageTeaserContext['imageId']) {
        $imageTeaserContext['imagePath'] = getFirstCoverImagePathFromBlocks($post);
    }

    // Und wenn es dann immer noch kein Bild gibt, greift der Fallback
    if (!$imageTeaserContext['imageId'] && !$imageTeaserContext['imagePath']) {
        $imageTeaserContext['imagePath'] = 'theme/fallback.jpg';
    }

    return Timber::compile('_teaser-' . $type . '.html.twig', $imageTeaserContext);
}


/**
 * Helper-Funktion um an das erste Bild des Artikels zu kommen
 */
function getFirstImageIdFromBlocks($post) {
    if (has_blocks($post->post_content)) {
        $blocks = parse_blocks($post->post_content);

        foreach ($blocks as $block) {
            if ($block['blockName'] === 'core/image') {
                return $block['attrs']['id'];
            }
        }

        return null;
    }
}

/**
 * Helper-Funktion um an das erste Cover eines Embeds im Artikel zu kommen
 */
function getFirstCoverImagePathFromBlocks($post) {
    if (has_blocks($post->post_content)) {
        $blocks = parse_blocks($post->post_content);

        foreach ($blocks as $block) {
            if ($block['blockName'] === 'core/embed') {
                $url = $block['attrs']['url'];
                $providerName = $block['attrs']['providerNameSlug'];
                $transientNameHash = wp_hash('hakerpsson_' . $providerName . '_' . $url);
                $transient         = get_transient($transientNameHash);

                if ($transient) {
                    if ($providerName === 'spotify') {
                        return $providerName . '/' . $transient->thumbnail_url;
                    }
                    
                    if ($providerName === 'vimeo') {
                        return $providerName . '/' . $transient->video_id;
                    }
                    
                    if ($providerName === 'youtube') {
                        return $providerName . '/' . $transient->items[0]->id;
                    }
                }
            }
        }

        return null;
    }
}
