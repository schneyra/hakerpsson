// https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API
// https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties

(() => {
    const intersectingElements = document.querySelectorAll(".js-intersection-ratio, .js-is-intersecting");

    if (intersectingElements.length) {

        let threshold = [];
        for (let i = 0; i <= 1.0; i += 0.01) {
            threshold.push(i);
        }

        let callback = (entries, observer) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    if (entry.target.classList.contains('js-intersection-ratio')) {
                        const intersectionRatio = entry.intersectionRatio.toFixed(2);
                        entry.target.style.setProperty("--intersectionRatio", intersectionRatio);
                    }

                    if (entry.target.classList.contains('js-is-intersecting') && entry.intersectionRatio.toFixed(2) >= 0.5) {
                        entry.target.classList.add("is-intersecting");
                    }
                }

                // Each entry describes an intersection change for one observed
                // target element:
                //   entry.boundingClientRect
                //   entry.intersectionRatio
                //   entry.intersectionRect
                //   entry.isIntersecting
                //   entry.rootBounds
                //   entry.target
                //   entry.time
            });
        };

        let options = {
            root: null,
            rootMargin: "0px",
            threshold: threshold
        };

        let observer = new IntersectionObserver(callback, options);

        intersectingElements.forEach((element) => {
            observer.observe(element);
        });
    }
})();
