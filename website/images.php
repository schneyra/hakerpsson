<?php
/**
 * Generates Image-Sizes with Cloudinary
 * 
 * Parameters:
 * /images/[width]/[height]/[blur]/[filepath with suffix].[jpg|webp]
 * 
 * Example:
 * https://hakerpsson.dertagundich.de/images/300/0/500/2020/10/DSC05802.jpg.webp
 * 
 * Necessary modifications to .htaccess:
 *      <IfModule mod_rewrite.c>
 *      RewriteEngine On
 *      RewriteCond images.php -f
 *      RewriteRule ^images images.php [L]
 *      </IfModule>
 */

$path = pathinfo($_SERVER['SCRIPT_NAME']);
$host = $_SERVER['HTTP_HOST'];
$protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';

if ($path['dirname'] != "/") {
    $params = str_replace($path['dirname'], '', $_SERVER['REQUEST_URI']);
} else {
    $params = $_SERVER['REQUEST_URI'];
}

preg_match("/\/([0-9]*)\/([0-9]*)\/([0-9]*)\/(.*)\.(jpg|webp|avif)/", $params, $paramsArray);

$originalFilePath = $paramsArray[0];

if (strpos($originalFilePath, 'mapbox/') > -1) {
    $localFilePath = 'cloudinary-images/mapbox/' . md5($originalFilePath) . '.' . $paramsArray[5];
} elseif (strpos($originalFilePath, 'spotify/') > -1) {
    $localFilePath = 'cloudinary-images/spotify/' . md5($originalFilePath) . '.' . $paramsArray[5];
} else {
    $localFilePath = 'cloudinary-images' . $originalFilePath;
}

// if the file is not yet cached
if (!file_exists($localFilePath)) {
    // set cloudinary-parameters
    $cloudinaryParams = [];
    $cloudinaryParams['quality'] = 'q_auto';
    $cloudinaryParams['width'] = $paramsArray[1] ? 'w_' . $paramsArray[1] : null;
    $cloudinaryParams['height'] = $paramsArray[2] ? 'h_' . $paramsArray[2] : null;
    $cloudinaryParams['blur'] = $paramsArray[3] ? 'e_blur:' . $paramsArray[3] : null;
    $cloudinaryParams['format'] = 'f_' . $paramsArray[5];

    // if width and height are set, we are going to crop 
    if ($cloudinaryParams['width'] && $cloudinaryParams['height']) {
        $cloudinaryParams[] = 'c_thumb';
        $cloudinaryParams[] = 'g_auto';
    }

    // Cloudinary has a limited height of 2160px for AVIF images
    if ($cloudinaryParams['width'] && !$cloudinaryParams['height']) {
        $cloudinaryParams[] = 'h_2160';
        $cloudinaryParams[] = 'c_limit';
    }

    // generate URL to get image from cloudinary
    $cloudinaryParamString = implode(',', array_filter($cloudinaryParams));
    $dtuiFilePath = $paramsArray[4];
    $fileTypeExtension = $paramsArray[5];

    if (strpos($dtuiFilePath, 'mapbox/') !== false) {
        $dtuiFilePath = str_replace('mapbox/', '', $dtuiFilePath);
        $dtuiFilePath = "https://api.mapbox.com/styles/v1/mapbox/streets-v11/static/" . $dtuiFilePath . "@2x?access_token=pk.eyJ1Ijoic2NobmV5cmEiLCJhIjoiY2l2NGFyajRhMDAwMjJ6cW1hdnRhem1sZiJ9.fytZp1WXg4T1n_AmngSPJA";
        $dtuiFilePath = urlencode($dtuiFilePath);
        $cloudinaryUrl = "https://res.cloudinary.com/schneyra/image/fetch/{$cloudinaryParamString}/{$dtuiFilePath}";
        //var_dump($cloudinaryUrl);
    } elseif (strpos($dtuiFilePath, 'youtube/') !== false) {
        $dtuiFilePath = str_replace('youtube/', '', $dtuiFilePath);
        $cloudinaryUrl = "https://res.cloudinary.com/schneyra/image/youtube/{$cloudinaryParamString}/{$dtuiFilePath}";
    } elseif (strpos($dtuiFilePath, 'vimeo/') !== false) {
        $dtuiFilePath = str_replace('vimeo/', '', $dtuiFilePath);
        $cloudinaryUrl = "https://res.cloudinary.com/schneyra/image/vimeo/{$cloudinaryParamString}/{$dtuiFilePath}";
    } elseif (strpos($dtuiFilePath, 'spotify/') !== false) {
        $dtuiFilePath = str_replace('spotify/', '', $dtuiFilePath);
        $cloudinaryUrl = "https://res.cloudinary.com/schneyra/image/fetch/{$cloudinaryParamString}/{$dtuiFilePath}";
    } elseif (strpos($dtuiFilePath, 'theme/') !== false) {
        $dtuiFilePath = str_replace('theme/', '', $dtuiFilePath);
        $imageFileUrl = "{$protocol}://{$host}/wp-content/themes/hakerpsson/images/{$dtuiFilePath}";

        if (strpos($imageFileUrl, 'http://localhost') > -1 ) {
            $cloudinaryUrl = $imageFileUrl;
        } else {
            $cloudinaryUrl = "https://res.cloudinary.com/schneyra/image/fetch/{$cloudinaryParamString}/{$imageFileUrl}";
        }
    } else {
        $imageFileUrl = "{$protocol}://{$host}/wp-content/uploads/{$dtuiFilePath}";

        if (strpos($imageFileUrl, 'http://localhost') > -1 ) {
            $cloudinaryUrl = $imageFileUrl;
        } else {
            $cloudinaryUrl = "https://res.cloudinary.com/schneyra/image/fetch/{$cloudinaryParamString}/{$imageFileUrl}";
        }
    }

    // get image
    $image = file_get_contents($cloudinaryUrl);

    if ($image) {
        // create folder structure if needed
        if (!file_exists(dirname($localFilePath))) {
            mkdir(dirname($localFilePath), 0777, true);
        }

        // save image
        file_put_contents($localFilePath, $image);
    } else {
        // the file was not delivered by cloudinary
        // exit here
        error_log("Bild nicht geladen: " . $cloudinaryUrl, 0);
        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
        exit();
    }
}

// set header and return image
$imageInfo = getimagesize($localFilePath);

// as of today, getimagesize() does not recognize avif images
// so we need to set the mimetype manually
$contentType = "image/avif";

// set the correct mimetype if getimagesize() recognizes the image
if ($imageInfo) {
    $contentType = $imageInfo['mime'];
}

header("content-type: {$contentType}");
header("cache-control: max-age=31536000, immutable");

readfile($localFilePath);
