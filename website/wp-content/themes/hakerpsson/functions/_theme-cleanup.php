<?php
if (!is_admin() ) {
	// WordPress-Skripte und -Styles entfernen
	add_action('wp_print_styles', function () {
		wp_deregister_script( 'wp-embed' );
		wp_dequeue_style( 'wp-block-library' );
	}, 100 );

	// Emoji-Quatsch entfernen
	add_action( 'init', function () {
		// all actions related to emojis
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
		remove_action( 'wp_head', 'wp_oembed_add_host_js' );

		add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
	} );
	
	add_filter( 'emoji_svg_url', '__return_false' );

	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'wp_shortlink_wp_head' );

	remove_filter( 'term_description', 'wpautop' );
	remove_action( 'wp_head', 'rest_output_link_wp_head' );
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
	remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
	remove_action( 'wp_head', 'wp_resource_hints', 2 );

	add_filter( 'the_generator', function() { return ''; } );

	add_action( 'wp_head', function () {
		echo '<link rel="alternate" type="application/rss+xml" title="RSS 2.0 Feed" href="' . get_bloginfo( 'rss2_url' ) . '" />';
	} );

    remove_action('init', 'register_block_core_gallery');
}