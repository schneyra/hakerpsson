<?php

use Timber\Menu;
use Timber\Timber;

$context = Timber::context();

$context['footerMenu'] = new Menu( 'footer' );

Timber::render( '_footer.html.twig', $context );
