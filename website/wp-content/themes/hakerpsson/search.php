<?php

use Timber\PostQuery;
use Timber\Timber;

$context                = Timber::context();
$context['posts']       = new PostQuery();
$context['queryTerm']   = get_search_query();
$context['currentPage'] = get_query_var('paged') ? get_query_var('paged') : 1;

Timber::render( 'search.html.twig', $context );
