<?php 

if (!is_admin()) {
    add_filter('render_block_core/archives', function ($content, $block) {
		if (is_feed()) {
			return $content;
		}

        if (isset($block['attrs']['className']) && strpos('is-style-chart', $block['attrs']['className']) > -1) {
            return renderArchivesChartBlock($content, $block);
        }

        return $content;
	}, 10, 2);
}

function renderArchivesChartBlock($content, $block) {
    loadChartsCss();

    $context = [];
    $context['archives'] = new TimberArchives();
    $context['yearWithMostPosts'] = ['post_count' => null];
    $context['monthWithMostPosts'] = ['post_count' => null];

    // Statistik über alle Jahre und Monate
    foreach ($context['archives']->items as $year) {        
        if ($context['yearWithMostPosts']['post_count'] < $year['post_count']) {
            $context['yearWithMostPosts'] = ['year' => $year['name'], 'post_count' => $year['post_count']]; 
        }

        foreach ($year['children'] as $month) {
            if ($context['monthWithMostPosts']['post_count'] < $month['post_count']) {
                $context['monthWithMostPosts'] = ['month' => $month['name'], 'year' => $year['name'], 'link' => $month['link'], 'post_count' => $month['post_count']]; 
            }
        }
    }

    // Statistik für ein Jahr, zur Berechnung der Balkenlänge
    $context['archives']->items = array_map(function($year) { 
        $year['mostPostsThisYear'] = ['post_count' => null];

        $year['children'] = array_reverse($year['children']);

        foreach ($year['children'] as $month) {
            if ($year['mostPostsThisYear']['post_count'] < $month['post_count']) {
                $year['mostPostsThisYear'] = ['name' => $month['name'], 'link' => $month['link'], 'post_count' => $month['post_count']]; 
            }
        }

        return $year;
    }, $context['archives']->items);
    
    return Timber::compile('_gutenberg-archives-chart.html.twig', $context);
};
