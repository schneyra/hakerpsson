<?php 

if (!is_admin()) {
    add_filter('render_block_core/image', function ($content, $block) {
		if (is_feed()) {
			return $content;
		}

        return renderImageBlock($content, $block);
	}, 10, 2);
}

function renderImageBlock($content, $block) {
    $image = array();

    // get ID from attributes
    $image['id']    = $block['attrs']['id'];
    $image['align'] = null;

    $image['className'] = $block['attrs']['className'] ?? '';

    if (isset($block['attrs']['className']) && strpos($block['attrs']['className'], 'is-style-fancy-image') > -1) {
        $image['className'] .= ' js-is-intersecting';
        enqueueIntersectionObserver();
    }

	$image['halfsize'] = false;

	if (isset($block['attrs']['align'])) {
		$image['className'] .= ' align' . $block['attrs']['align'];
		$image['align'] = $block['attrs']['align'];

		if ($block['attrs']['align'] === 'left' || $block['attrs']['align'] === 'right') {
			$image['halfsize'] = true;
		}
    }

    // Get caption from content
    $dom = new DOMDocument();
    libxml_use_internal_errors( true );
    $dom->loadHTML( mb_convert_encoding( $content, 'HTML-ENTITIES', 'UTF-8' ), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );
    libxml_use_internal_errors( false );

    $image['caption'] = '';
    foreach ($dom->getElementsByTagName('figcaption') as $node) {
        $image['caption'] = $node->textContent;
    }

    $image['alt'] = '';
    foreach ($dom->getElementsByTagName('img') as $node) {
        $image['alt'] = $node->getAttribute('alt');
    }

    $imageAttributes = wp_get_attachment_image_src($image['id'], 'full');

    $image['width']  = $imageAttributes[1];
    $image['height'] = $imageAttributes[2];

    $context          = [];
    $context['image'] = $image;


    return Timber::compile('_gutenberg-image.html.twig', $context);
};
