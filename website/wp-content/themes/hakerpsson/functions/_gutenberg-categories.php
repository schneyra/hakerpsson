<?php 

if (!is_admin()) {
    add_filter('render_block_core/categories', function ($content, $block) {
		if (is_feed()) {
			return $content;
		}

        if (isset($block['attrs']['className']) && strpos("is-style-chart", $block['attrs']['className']) > -1) {
            return renderCategoriesChartBlock($content, $block);
        }

        return $content;
	}, 10, 2);
}

function renderCategoriesChartBlock($content, $block) {
    loadChartsCss();

    $context = [];
    $context['sumOfPosts'] = 0;
    $context['mostPostsInCategory'] = 0;

    $context['categories'] = [];
    $context['archiveCategory'] = null;

    foreach(get_categories() as $category) {
        if ($category->slug === 'archiv') {
            $context['archiveCategory'] = [
                'name' => $category->name,
                'count' => $category->count,
                'link' => get_category_link($category)
            ];
        } else {
            $context['sumOfPosts'] += $category->count;
            if ($context['mostPostsInCategory'] < $category->count) {
                $context['mostPostsInCategory'] = $category->count;
            }

            if ($category->category_parent) {
                $context['categories'][$category->category_parent] = $context['categories'][$category->category_parent] ?? [];
                $context['categories'][$category->category_parent]['children'][$category->term_id] = [
                    'name' => $category->name,
                    'count' => $category->count,
                    'link' => get_category_link($category),
                ];
            } else {
                $context['categories'][$category->term_id] = $context['categories'][$category->term_id] ?? [];
                $context['categories'][$category->term_id] = array_merge($context['categories'][$category->term_id], [
                    'name' => $category->name,
                    'count' => $category->count,
                    'link' => get_category_link($category),
                ]);
            }
        }
    }

    return Timber::compile('_gutenberg-categories-chart.html.twig', $context);
};

function loadChartsCss() {
    if (file_exists(get_template_directory() . '/dist/manifest.json')) {
		$manifest = file_get_contents(get_template_directory() . '/dist/manifest.json');
		$manifest = json_decode($manifest, true);

		if (isset( $manifest['css/charts.min.css'])) {
			$cssFileUrl = get_template_directory_uri() . '/dist/' . $manifest['css/charts.min.css'];
			wp_enqueue_style('charts', $cssFileUrl);
		}
	}
}
