(() => {
    const flymap = window.flymap;

    if (flymap) {
        // initiales Setup der Karte
        mapboxgl.accessToken = 'pk.eyJ1Ijoic2NobmV5cmEiLCJhIjoiY2l2NGFyajRhMDAwMjJ6cW1hdnRhem1sZiJ9.fytZp1WXg4T1n_AmngSPJA';
        let map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
        });

        // Zoomen durch Scrollen deaktivieren
        map.scrollZoom.disable();

        const removeActiveMarker = () => {
            const oldActiveMarker = document.querySelector('.is-active-marker');
            if (oldActiveMarker) {
                oldActiveMarker.classList.remove('is-active-marker');
            }
        }

        // Klasse auf Marker setzen, damit man diesen umfärben kann
        const setActiveMarker = (id) => {
            removeActiveMarker();

            const newActiveMarker = document.querySelector(`[data-id="${id}"]`);
            if (newActiveMarker) {
                newActiveMarker.classList.add('is-active-marker');
            }
        }

        // Marker einfügen
        const poiForMap = flymap.poiForMap;

        let markers = [];
        for (const [id, data] of Object.entries(poiForMap)) {
            const newMarker = new mapboxgl.Marker()
            .setLngLat(data.center)
            .addTo(map);

            newMarker.getElement().dataset.id = data.id;
            newMarker.getElement().addEventListener('click', () => {
                setActiveMarker(data.id);
                const listElement = document.getElementById(data.id);
                listElement.parentNode.scrollTop = listElement.offsetTop;
            });

            markers.push(newMarker);
        }

        // Außenabstand setzen
        const setPadding = () => {
            const paddingRight = window.matchMedia('(min-width: 1200px)').matches ? 700 : 50; 
            const paddingBottom = window.matchMedia('(min-width: 1200px)').matches ? 50 : 275; 
            
            map.easeTo({
                padding: {
                    top: 75,
                    right: paddingRight,
                    bottom: paddingBottom,
                    left: 75
                },
                duration: 0
            });
        }

        setPadding();
        window.addEventListener('resize', setPadding);
        
        // Karte initial so zoomen, dass alle Marker angezeigt werden
        const setOverview = () => {

            let bounds = new mapboxgl.LngLatBounds();
            flymap.bounds.forEach((item) => {
                bounds.extend(item);
            });
            map.fitBounds(bounds);
        }

        setOverview();

        // Navigationselemente (Zoom, Kompass)
        const nav = new mapboxgl.NavigationControl();
        map.addControl(nav, 'top-left');

        // Liste zum Navigieren der Karte
        const intersectingElements = document.querySelectorAll(".js-is-active-poi");
        if (intersectingElements.length) {
            let threshold = [];
            for (let i = 0; i <= 1.0; i += 0.01) {
                threshold.push(i);
            }

            let callback = (entries, observer) => {
                entries.forEach((entry) => {
                    if (entry.isIntersecting) {
                        if (entry.intersectionRatio.toFixed(2) >= 0.9 && !entry.target.classList.contains('is-active-poi')) {
                            // Listenelement aktiv setzen
                            document.querySelector('.is-active-poi').classList.remove('is-active-poi');
                            entry.target.classList.add('is-active-poi');

                            // Karte bewegen
                            const poiId = entry.target.dataset.poiId;

                            if (poiId === 'overview') {
                                setOverview();
                                removeActiveMarker();
                            } else {
                                setActiveMarker(poiForMap[poiId].id);
                                map.flyTo(poiForMap[poiId]);
                            }

                            // Status der Hoch- und Runter-Buttons setzen 
                            const scrollbuttons = document.querySelectorAll('.js-poi-scrollbutton');
                            if (scrollbuttons) {
                                scrollbuttons.forEach((button) => {
                                    button.setAttribute('aria-disabled', 'false');

                                    if (button.dataset.direction === 'up' && !entry.target.previousElementSibling) {
                                        button.setAttribute('aria-disabled', 'true');
                                    } else if (button.dataset.direction === 'down' && !entry.target.nextElementSibling) {
                                        button.setAttribute('aria-disabled', 'true');
                                    }
                                })
                            }
                        }
                    }
                });
            };

            let options = {
                root: null,
                rootMargin: "0px",
                threshold: threshold
            };

            let observer = new IntersectionObserver(callback, options);

            intersectingElements.forEach((element) => {
                observer.observe(element);
            });
        }
    }
})();

(() => {
    const scrollbuttons = document.querySelectorAll('.js-poi-scrollbutton');

    if (scrollbuttons) {
        scrollbuttons.forEach((button) => {
            button.addEventListener('click', (event) => {
                if (event.target.getAttribute('aria-disabled') === true) {
                    return;
                }

                const activePoi = document.querySelector('.is-active-poi');
                let slideToPoiId = null;

                if (event.target.dataset.direction === 'up') {
                    if (activePoi.previousElementSibling) {
                        slideToPoiId = activePoi.previousElementSibling.getAttribute('id');
                    }
                } else {
                    if (activePoi.nextElementSibling) {
                        slideToPoiId = activePoi.nextElementSibling.getAttribute('id');
                    }
                }

                if (slideToPoiId) {
                    const slideToPoiElement = document.getElementById(slideToPoiId);
                    slideToPoiElement.parentNode.scrollTop = slideToPoiElement.offsetTop;
                    slideToPoiElement.focus();
                }
            });
        });
    }
})();
