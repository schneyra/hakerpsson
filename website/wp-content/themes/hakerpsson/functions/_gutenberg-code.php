<?php

if (!is_admin()) {
	add_filter('render_block_core/code', function ($content, $block) {
		if (is_feed()) {
			return $content;
		}

		loadPrismFiles();

		return $content;
	}, 10, 2);
}

function loadPrismFiles() {
	if (file_exists(get_template_directory() . '/dist/manifest.json')) {
		$manifest = file_get_contents(get_template_directory() . '/dist/manifest.json');
		$manifest = json_decode($manifest, true);

		if (isset( $manifest['js/prism.min.js'])) {
			$jsFileUrl = get_template_directory_uri() . '/dist/' . $manifest['js/prism.min.js'];
			wp_enqueue_script('prism', $jsFileUrl, [], null, true);
		}

		if (isset( $manifest['css/prism.min.css'])) {
			$cssFileUrl = get_template_directory_uri() . '/dist/' . $manifest['css/prism.min.css'];
			wp_enqueue_style('prism', $cssFileUrl);
		}
	}
}