<?php

// remove block styles
add_action('after_setup_theme', function() {
    // remove predefined block patterns ("Vorlagen")
    remove_theme_support('core-block-patterns');
});
 
// Remove default styles from blocks
// https://wordpress.stackexchange.com/questions/352559/remove-block-styles-in-the-block-editor
add_action('init', function () {
    if (is_admin()) {
        add_action('admin_enqueue_scripts', function () {
            if (file_exists(get_template_directory() . '/dist/manifest.json')) {
                $manifest = file_get_contents( get_template_directory() . '/dist/manifest.json' );
                $manifest = json_decode( $manifest, true );

                if (isset( $manifest['js/gutenberg-config.min.js'])) {
                    $jsFileUrl = get_template_directory_uri() . '/dist/' . $manifest['js/gutenberg-config.min.js'];
                    wp_enqueue_script('removeStyles', $jsFileUrl, ['wp-blocks', 'wp-edit-post'], null, true);
                }
            }
        } );
    }
});

register_block_style(
  'core/image',
   array(
     'name'  => 'fancy-image',
     'label' => __( 'Fancy Image', 'fancy-image' ),
   )
   );

register_block_style(
    'core/image',
    array(
        'name'  => 'postcard-image',
        'label' => __( 'Postkarte', 'postcard-image' ),
   )
);

register_block_style(
    'core/gallery',
    array(
        'name'  => 'postcard-gallery',
        'label' => __( 'Postkarten', 'postcard-gallery' ),
   )
);

register_block_style(
    'core/categories',
    array(
        'name'  => 'chart',
        'label' => __( 'Chart', 'chart' ),
   )
);

register_block_style(
    'core/archives',
    array(
        'name'  => 'chart',
        'label' => __( 'Chart', 'chart' ),
   )
);
