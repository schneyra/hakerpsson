<?php

use Timber\PostQuery;
use Timber\Timber;
use Timber\Term;

$context                = Timber::context();
$context['posts']       = new PostQuery();
$context['term']        = new Term();
$context['currentPage'] = get_query_var('paged') ? get_query_var('paged') : 1;

$poiForMap = [];
$sections = [];
$bounds = [];

foreach ($context['posts'] as $post) {
    if (has_blocks($post->post_content)) {
        $blocks = parse_blocks($post->post_content);

        foreach ($blocks as $block) {
            if ($block['blockName'] === 'carbon-fields/poi') {
                $data = $block['attrs']['data'];
                $lnglat = $data['map']['lng'] . ",". $data['map']['lat'];
                $id = sanitize_title($lnglat . '-' . $post->ID);

                $poiForMap[$id] = [
                    'bearing' => rand(0, 180),
                    'center' => [$data['map']['lng'], $data['map']['lat']],
                    'zoom' => rand(12, 17),
                    'pitch' => rand(0,40),
                    'id' => $id,
                ];

                $poiForList[] = [
                    'id' => $id,
                    'title' => $data['title'],
                    'postTitle' => $post->post_title,
                    'description' => $data['description'],
                    'link' => get_permalink($post) . '#' . sanitize_title($data['title'])
                ];

                $bounds[] = [$data['map']['lng'], $data['map']['lat']];
            }
        }
    }
};

$context['flymap'] = null;

if (count($poiForMap) && count($poiForList)) { 
    wp_enqueue_style('mapbox', 'https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.css');
    wp_enqueue_script('mapbox', 'https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.js', [], null, true);

	if (file_exists(get_template_directory() . '/dist/manifest.json')) {
		$manifest = file_get_contents(get_template_directory() . '/dist/manifest.json');
		$manifest = json_decode($manifest, true);

		if (isset($manifest['js/poi-map.min.js'])) {
				$jsFileUrl = get_template_directory_uri() . '/dist/' . $manifest['js/poi-map.min.js'];
				wp_enqueue_script('poi-map', $jsFileUrl, ['mapbox'], null, true);
		}
	}

    $context['flymap'] = [
        'poiForMap' => $poiForMap,
        'poiForList' => $poiForList,
        'bounds' => $bounds,
        'first' => $poiForMap[$poiForList[0]['id']]
    ];
}

Timber::render('category.html.twig', $context);
