<?php
add_action( 'wp_enqueue_scripts', function() {
	if ( file_exists( get_template_directory() . '/dist/manifest.json' ) ) {
		$manifest = file_get_contents( get_template_directory() . '/dist/manifest.json' );
		$manifest = json_decode( $manifest, true );

		if ( isset( $manifest['css/hakerpsson.min.css'] ) ) {
			$cssFileUrl = get_template_directory_uri() . '/dist/' . $manifest['css/hakerpsson.min.css'];
			wp_enqueue_style( 'hakerpsson', $cssFileUrl );
		}

		if ( isset( $manifest['js/instantpage.min.js'] ) ) {
			$jsFileUrl = get_template_directory_uri() . '/dist/' . $manifest['js/instantpage.min.js'];
			wp_enqueue_script('instantpage', $jsFileUrl, [], null, true);
		}

		enqueueIntersectionObserver();
	}
});

if (is_admin()) {
	add_action( 'admin_enqueue_scripts', function () {
		if ( file_exists( get_template_directory() . '/dist/manifest.json' ) ) {
			$manifest = file_get_contents( get_template_directory() . '/dist/manifest.json' );
			$manifest = json_decode( $manifest, true );

			if ( isset( $manifest['css/gutenberg.min.css'] ) ) {
				$cssFileUrl = get_template_directory_uri() . '/dist/' . $manifest['css/gutenberg.min.css'];
				wp_enqueue_style( 'hakerpsson-gutenberg', $cssFileUrl );
			}
		}
	});
}

if (!is_admin()) {
	// WordPress-Skripte und -Styles entfernen
	add_action('wp_print_styles', function () {
		wp_deregister_script('wp-embed');
		wp_dequeue_style('wp-block-library');
		wp_dequeue_style('global-styles');
	}, 100);

    // Scripte 'defer' laden und 'type' entfernen, damit der W3-Validator glücklich ist
	add_filter('script_loader_tag', function ($tag) {
        $tag = str_replace( array(
            ' src',
            "type='text/javascript'"
        ), array(
            ' defer src',
            ""
        ), $tag);

        return $tag;
	}, 10, 2);

	/**
	 * Entferne die Versionsnummer von JS- und CSS-Dateien
	 */
	add_filter('style_loader_src', 'removeVersion', 10);
	add_filter('script_loader_src', 'removeVersion', 10);

	function removeVersion($src) {
		if (strpos($src, 'ver=')) {
			$src = remove_query_arg('ver', $src);
		}

		return $src;
	}
}

function enqueueIntersectionObserver() {
	if (file_exists(get_template_directory() . '/dist/manifest.json')) {
		$manifest = file_get_contents(get_template_directory() . '/dist/manifest.json');
		$manifest = json_decode($manifest, true);

		if (isset($manifest['js/intersection.min.js'])) {
				$jsFileUrl = get_template_directory_uri() . '/dist/' . $manifest['js/intersection.min.js'];
				wp_enqueue_script('intersection', $jsFileUrl, [], null, true);
		}
	}
}